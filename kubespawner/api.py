"""A API handler to fetch a list of available profiles"""

from jupyterhub.apihandlers import  APIHandler, default_handlers

class KubeSpawnerAPIHandler(APIHandler):

    def get(self):

        profiles = self.config.KubeSpawner.profile_list

        if not profiles or callable(profiles):
            profiles = [ ]
            
        self.write({ 'profiles': profiles })

default_handlers.append((r"/api/kubespawner/profiles", KubeSpawnerAPIHandler))
